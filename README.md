# React Advanced Example

An advanced React.js sandbox project. You can modify the files inside `src` folder and then run the following commands:

```bash
npm run transpilation
npm start
```

After that, point your browser to the URLs listed in your CLI.
