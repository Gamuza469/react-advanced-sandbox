import Greeting from './components/Greeting.js';

ReactDOM.render(
    <Greeting name="User" />,
    document.getElementById('root')
);
