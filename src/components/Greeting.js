class Greeting extends React.Component {
    render() {
        const {name} = this.props;

        return (
            <div>
                Hello there, Mr. {name}!
            </div>
        );
    }
}

Greeting.defaultProps = {
    name: 'default user'
};

export default Greeting;
